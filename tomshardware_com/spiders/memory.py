# -*- coding: utf-8 -*-
import re
import scrapy


class MemorySpider(scrapy.Spider):
    name = 'memory'
    allowed_domains = ['tomshardware.com', 'tomshardware.co.uk']
    start_urls = ['http://www.tomshardware.co.uk/forum/forum-75.html']


    def parse(self, response):
        items_css = 'tr.thread.thread-status-solved'
        items = response.css(items_css)

        if len(items) == 0:
            return

        for item in items:
            # status = item.css('tr.thread.thread-status-solved').extract_first()
            # if not status:
            #     continue

            url = item.css('a.thread-link ::attr(href)').extract_first()
            answers = item.css('span.thread-replies ::text').extract_first()

            try:
                answers = int(answers.strip())
                if answers < 1:
                    continue
            except:
                self.logger.warning('Unable to convert \'answers\' to int'.format(answers))
                continue

            yield response.follow(url, callback=self.parse_item)

        pages_css = 'ul.pager li.deskon a ::attr(href)'
        pages = response.css(pages_css).extract()

        if not pages:
            pages_css = 'ul.pager li.deskon span ::text'
            pages = response.css(pages_css).extract()

            u = 'http://www.tomshardware.co.uk/forum/forum-75/page-{}.html'
            pages = [u.format(p) for p in pages if re.match('[0-9]+', p.strip())]

        for page in pages:
            yield response.follow(page, callback=self.parse)


    def parse_item(self, response):
        d = {}

        d['Url'] = response.url
        d['QuestionTitle'] = response.css('h1.thread__title ::text').extract_first()
        d['QuestionBody'] = response.css('div.thread__content').extract_first()
        d['QuestionTags'] = [t.strip().lower() for t in response.css('div.tags-list span ::text').extract()]

        for answers in response.css('div.thread__answers'):
            if answers.css('span.bestanswer__header'):
                d['AnswersBody'] = response.css('div.answer__content').extract_first()
                return d

        # self.logger.warning('Unable to find best answer at <{}>'.format(response.url))
